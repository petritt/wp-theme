<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Novasolutions
 */

?>

	<footer id="colophon" role="contentinfo">


		<?php if( ! is_page( array( 'contact', 'test' ) ) ) { ?>

			  <div class="h-275"></div>
                <div class="footer-top position-relative">
                    <a href="/contact">
                        <div class="row image-foo">
                            <div class="col-lg-6 offset-lg-6">
                                <h2>Get In Touch</h2>
                                <p>Let's make your puzzle together / Do you have puzzle for us? </p>
                                <div class="arrow-right">
                                    <i class="fas fa-arrow-right"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <!-- .site-info -->

 		<?php } ?>

		<div class="footer-bottom">
			<div class="row py-5">
				<div class="col-12 col-md-6">
					<div class="col-9 logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/Logo_Footer.png"
                                 alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                            />
                        </a>

                    </div>
					<div class="col-9 pt-4">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque tempus ullamcorper magna, ac cursus purus consectetur ut. Nam imperdiet, est id vehicula porttitor,
						</p>
					</div>
				</div>
				<div class="col-12 col-md-6 align-self-center">
					<div class="footer-menu">
						<?php
							wp_nav_menu(
								array(
								'theme_location' => 'menu-1',
								'menu_class'     => 'pl-0 align-items-center mb-0',
								)
							);
						?>
					</div>
					<div class="social pt-4">
						<ul class="list-inine list-unstyled mb-0">
							<li class="list-inline-item py-1">
                                <a href="https://www.facebook.com/nova.solutions" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
							<li class="list-inline-item py-1">
                                <a href="https://www.instagram.com/nova_solutions/" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
							<li class="list-inline-item py-1">
                                <a href="https://www.linkedin.com/company/nova-solutions/" target="_blank">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
						</ul>
					</div>
					<div class="privacy pt-4">
						<li class="list-inline-item py-1"><a href="#">Privacy Policy</a></li>
						<li class="list-inline-item py-1"><a href="#">Tearms & Conditions</a></li>
					</div>
					
				</div>

			</div>

		</div>

		<div class="madeby py-3">
			<div class="row">
				<div class="col-12 col-md-6">
					<p class="mb-0">All rights reserved © Copyright NovaSolutions 2020.</p>
				</div>
				<div class="col-12 col-md-6 text-md-right">
					<p class="mb-0">Made By: Nova Solutons</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
