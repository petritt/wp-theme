jQuery(document).ready(function ($) {

    /** =============================================================== **/
    /** Menu toogler on mobile hamburger class change **/
    /** =============================================================== **/

    $('.hamburger').on("click", function (e) {
        e.preventDefault();
        $(this).toggleClass('is-active');
    });  

    /** =============================================================== **/
    /** Slider Slick **/
    /** =============================================================== **/

        $('.products-sldier').slick({
        dots: false,
        infinite: true,
        arrows: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [       
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              arrows: false,
              dots: true
            }
          },
          {
            breakpoint: 583,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
    

        $('.big-slider').slick({
        dots: false,
        infinite: true,
        arrows: true,
        speed: 700,
        autoplay: true,
        autoplaySpeed: 3500,
        pauseOnFocus: false,
        pauseOnHover: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [       
          {
            breakpoint: 1199.98,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
    



    $('.sp-slider').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: true,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: true,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
      });
	  
	  
	  
	$('.single-item').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
		dots: true,
		// autoplay: true,
		arrows: false,
	});
	  
	  
	  
	$('.centerourwork').slick({
	  centerMode: true,
	  centerPadding: '80px',
	  slidesToShow: 2,
	  slidesToScroll: 1,
	  arrows: true,
	  focusOnSelect: true,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
	  responsive: [
	    {
	      breakpoint: 768,
	      settings: {
	        arrows: true,
	        centerMode: true,
	        centerPadding: '40px',
	        slidesToShow: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        arrows: true,
	        centerMode: true,
	        centerPadding: '40px',
	        slidesToShow: 1
	      }
	    }
	  ]
	});

});

function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

if(document.getElementById("divWpVue")) {

    var app = new Vue({
        el: '#divWpVue',
        data: {
            info: [],
            categories: [],
            category: 'All',
            loading: true,
        },
        created() {
            this.getArchives()
        },
        methods: {
            async getArchives() {
                this.loading = true;
                await axios.get('https://novasolutions.co/wp-json/wp/v2/portfolio?_embed&filter[orderby]=date&order=asc')
                    .then(response => {

                        for (let portfolio of response.data) {
                            if (portfolio._embedded !== undefined && portfolio._embedded["wp:featuredmedia"] !== undefined &&
                                portfolio._embedded["wp:featuredmedia"][0].media_details !== undefined ) {
                                this.info.push({
                                    'id': portfolio.id,
                                    'name': portfolio.title.rendered,
                                    'url': portfolio.slug,
                                    'image': portfolio._embedded["wp:featuredmedia"][0].media_details.sizes.full.source_url,
                                    'category': portfolio.categories
                                });
                            }


                        }
                    })
                    .catch(e => {
                        console.log(e)
                    }).finally(() => {
                        this.loading = false;
                    });

                await axios.get("https://novasolutions.co/wp-json/wp/v2/categories").then(response => {
                    for (let category of response.data) {
                        this.categories[category.id] = category.name
                    }
                })
                .catch(e => {
                    console.log(e)
                }).finally(() => {
                    this.loading = false;
                });

            },
            getImgUrl(url) {
                return url
            }
        },
        computed: {
            filteredServices() {
                let allServices = this.info;

                allServices.sort(function (a, b) {
                    return a.id - b.id;
                });

                if (this.category === 'All') {
                    return allServices;
                }

                return allServices.filter((service) => {
                    let searchCategories = [];
                    for (let category of service.category) {
                        searchCategories.push(this.categories[category]);
                    }
                    return `${searchCategories}`.toLowerCase().includes(this.category.toLowerCase());
                })
            }
        }

    })

    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > ($(document).height() - 450) ) {
            let element = document.getElementById("portfolio_sidebar");
            element.classList.remove("position-md-fixed");
        } else {
            let element = document.getElementById("portfolio_sidebar");
            element.classList.add("position-md-fixed")
        }
    });

}

