<?php
/**
 * Template Name: The team
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" class="container-fluid" role="main">
			<div class="row">
				<img src="<?= get_field('team_image_banner');?>">
			</div>
		</div><!-- #content -->
		<section class="container team-container">

            <div class="row">
                <div class="col-12">
                    <h1 class="text-orange text-right team-header-text">
                        <?= get_field('team_header_text'); ?>
                    </h1>
                </div>
                <div class="col-12">
                    <h2 class="text-orange text-right team-header-subtext">
                        <?= get_field('team_header_subtext'); ?>
                    </h2>
                </div>
            </div>

			<div class="row team-wrapper justify-content-center">
			
			<?php
				if( have_rows('team_employees') ):
					while ( have_rows('team_employees') ) : the_row();?>
			
				<div class="col-12 col-md-3 col-lg-4">
					<div class="card team-card team_wrap" style="padding: 5%;">
						<img class="card-img-top employee-image" src="<?= the_sub_field('employee_image'); ?>">
						<img class="card-img-top employee-image-hover" src="<?= the_sub_field('employee_image_hover'); ?>">
						<div class="card-body team-card-body">
							<h5 class="card-title text-orange pt-5"><?= the_sub_field('employee_name'); ?></h5>
							<small class="card-text text-orange"><?= the_sub_field('employee_position'); ?></small>
							<hr class="mt-4" />
							<p class="card-text text-orange pt-5"><b><?= the_sub_field('employee_count'); ?></b></p>
						</div>
					</div>
				</div>

				<?php endwhile;
				                else :
				                   // no rows found 
				                endif;
				            ?>
			</div>
		</section>
		
	</div><!-- #primary -->

<?php get_footer(); ?>
