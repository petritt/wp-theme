<?php
/**
 * Template Name: Services
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<div class="container">
				<div class="row service-slider">
					<div class="col-12 col-md-6 col-lg-6">
						<div class="row row-smaller h-100">
							<div class="col-12 col-smaller align-self-top pt-5">
								<h1 class="mb-3 mb-lg-4 text-orange"><?= get_field('heading_services');?></h1>
								<h2 class="text-orange"><?= get_field('paragraph_services');?></h2>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-6">
						<div class="image-wrap text-right image_slider">
							<img src="<?= get_field('banner_sercvices');?>">
						</div>
					</div>
				</div>
			</div>
			<div class="servicesCount">
				<div class="w-100 service" style="background-image: url('<?php the_field('background_first'); ?>');">
					<div class="service_wrap text-left text-white py-5">
						<div class="row py-5">
							<div class="col-12 col-md-7 mr-auto pl-5">
								<div class="py-0 pl-5">
									<span class="text-orange font-weight-bold mr-3 d-inline">01</span><h2 class="d-inline"><?php the_field('first_section_title'); ?></h2>
								</div>
								<div class="py-0 pl-5"><p><?php the_field('first_section_text'); ?></p></div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="w-100 service" style="background-image: url('<?php the_field('background_second'); ?>');">
					<div class="service_wrap text-right text-white py-5">
						<div class="row py-5">
							<div class="col-12 col-md-7 ml-auto pr-5">
								<div  class="py-0 pr-5">
									<h2 class="d-inline"><?php the_field('second_section_title'); ?></h2><span class="text-orange font-weight-bold ml-3 d-inline">02</span>
								</div>
                                <div class="py-0 pr-5"><p><?php the_field('second_section_text'); ?></p></div>
                            </div>
						</div>
					</div>
				</div>
				<div class="w-100 service" style="background-image: url('<?php the_field('background_third'); ?>');">
					<div class="service_wrap text-left text-white py-5">
						<div class="row py-5">
							<div class="col-12 col-md-7 mr-auto pl-5">
								<div class="py-0 pl-5">
									<span class="text-orange font-weight-bold mr-3 d-inline">03</span><h2 class="d-inline"><?php the_field('third_section_title'); ?></h2>
								</div>
                                <div class="py-0 pl-5"><p><?php the_field('third_section_text'); ?></p></div>
                            </div>
						</div>
					</div>
				</div>
				<div class="w-100 service" style="background-image: url('<?php the_field('background_fourth'); ?>');">
					<div class="service_wrap text-right text-white py-5">
						<div class="row py-5">
							<div class="col-12 col-md-7 ml-auto pr-5">
								<div class="py-0 pr-5">
								    <h2 class="d-inline"><?php the_field('fourth_section_title'); ?></h2><span class="text-orange font-weight-bold ml-3 d-inline">04</span>
								</div>
                                <div class="py-0 pr-5"><p><?php the_field('fourth_section_text'); ?></p></div>
                            </div>
						</div>
					</div>
				</div>
			</div>

			<div class="ourClients-services container">
				<h2 class="paddingbottom pt-5"><?php the_field('our_client_title'); ?></h2>
				
					<div class="paddingleft my-5 pb-5">
						<div class="row row-smaller align-items-center lastChild">
							<?php
				                if( have_rows('our_client_logos') ):
				                   	while ( have_rows('our_client_logos') ) : the_row();?>
										
										<div class="col-2dot4 col-sm-2dot4 col-md-2dot4 col-lg-2dot4 col-xl-2dot4 text-center col-smaller">
											<a href="<?php the_sub_field('client_link') ?>">
											<img class="logo-partners mb-5 pb-5" src="<?php the_sub_field('client_logo') ?>">
											</a>
										</div>
										
									<?php endwhile;
				                else :
				                   // no rows found
				                endif;
				            ?>
		            	</div>
					</div>
									
			
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
