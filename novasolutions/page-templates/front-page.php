<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" class="container" role="main">
			<div class="home_slider">
				<div class="row">
					<div class="col-12 col-md-7 col-lg-6">
						<div class="row row-smaller h-100 home_slider_text">
							<div class="col-12 col-smaller align-self-end">
								<h1 class="mb-3 mb-lg-4 text-orange heading-banner"><?php echo get_field('heading_banner');?></h1>
								<h2 class="text-orange"><?= get_field('heading_paragraph');?></h2>
							</div>
                            <div class="col-12 col-smaller align-self-end">
                                <div class="slider_links mb-4">
                                    <a href="/services" class="">
                                        <h4 class="slider mb-2 d-inline-block position_class">
                                            our services
                                        </h4>
                                    </a>
                                </div>
                                <div class="slider_links">
                                    <a href="/contact" class="">
                                        <h4 class="slider mb-2 d-inline-block position_class">
                                            contact
                                        </h4>
                                    </a>
                                </div>
                            </div>
						</div>
					</div>
					<div class="col-12 col-md-5 col-lg-6">
						<div class="image-wrap text-right position-absolute">
							<img src="<?= get_field('banner_image');?>">
						</div>
					</div>

				</div>
			</div>
		</div><!-- #content -->
		<section class="services-home">
			<div class="image-bg" style="background-image: url('<?php the_field('services_background'); ?>');">
				<div class="container h-100">
			
					<div class="row h-100">
						<div class="col-12 align-self-center">
							<h2 class="paddingbottom"><?php the_field('services_title'); ?></h2>
							<div class="paddingleft">
								<ul class="list-unstyled list-inine mb-0">
			                        <li class="mb-5">
			                           <h3> <span class="text-orange font-weight-bold mr-5">01</span><?php the_field('services_list_1'); ?></h3>
			                        </li>
			                        <li class="mb-5">
			                            <h3><span class="text-orange font-weight-bold mr-5">02</span><?php the_field('services_list_2'); ?></h3>
			                        </li>
			                        <li class="mb-5">
			                            <h3> <span class="text-orange font-weight-bold mr-5">03</span><?php the_field('services_list_3'); ?></h3>
			                       	</li>
			                       	<li class="mb-5">
			                            <h3><span class="text-orange font-weight-bold mr-5">04</span><?php the_field('services_list_4'); ?></h3>
			                       	</li>
		                   		</ul>
		                	</div>
		                	<div class="text-right paddingright mt-5">
                                <a href="/services">
                                    <h4 class="text-orange font-weight-bold d-inline-block position_class">
                                        Explore More
                                    </h4>
                                </a>
                            </div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<section class="testemonias pt-5">

			<div class="container py-5">
				<div class="row py-5">
					<div class="col-12 col-md-4">
						<h2 class="paddingleft"><?php the_field('testemonials_title')?></h2>
					</div>
					<div class="col-12 col-md-8">

						<div class="single-item">
							<?php
				                if( have_rows('testemonials') ):
				                   	while ( have_rows('testemonials') ) : the_row();?>
										<div class="">
											<p><?php the_sub_field('testemonials_desc') ?></p>
											<div class="mt-5">
												<p class="font-weight-bold d-inline-block mr-3">-<?php the_sub_field('testemonials_name') ?></p>
												<p class="font-weight-normal d-inline-block"><?php the_sub_field('testemonials_position') ?></p>
											</div>
										</div>	
									<?php endwhile;
				                else :
				                   // no rows found
				                endif;
				            ?>
						 
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="ourwork">
			<h2 class="paddingbottom text-grey pt-5 pl-5"><?php the_field('ourwork_title'); ?></h2>
				<div class="ourwork-inside">
					
						<div class="centerourwork">
							<?php


                                $rows = get_field('ourwork_slider');
                                $i = 1;
				                if( have_rows('ourwork_slider') ):

				                   	while ( have_rows('ourwork_slider') ) : the_row();?>
                                        <a href="/portfolio">
										<div class="portfolio_wrap">
											<div class="image_wrap">
												<img src="<?php the_sub_field('ourwork_images') ?>">
											</div>
											<div class="row py-5 description_warp">
												<div class="col-12 col-md-12 col-lg-6">
													<h4 class="text-orange font-weight-bold d-inline-block position_class">
                                                        <?php the_sub_field('ourwork_slider_title') ?>
                                                    </h4>
												</div>
												<div class="col-12 col-md-12 pt-xs-4 pt-sm-4 pt-md-4 pt-lg-0 col-lg-6">
													<p class="text-orange"><?php the_sub_field('ourwork_desc') ?></p>
												</div>

                                                <div class="col-12 col-md-6 col-lg-8">
                                                    <p class="card-text text-orange pt-md-2 pt-lg-3"><b>
                                                            <?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ?>
                                                            / <?php  echo str_pad(count($rows), 2, '0', STR_PAD_LEFT)  ?>
                                                    </b></p>
                                                </div>
											</div>
										</div>
                                        </a>
									<?php $i++; endwhile;


				                else :
				                   // no rows found
				                endif;
				            ?>
						</div>
				
				</div>
            <div class="paginator-center text-left">
                <i class="prev fas fa-chevron-left"></i>
                <i class="next fas fa-chevron-right"></i>
            </div>

		</section>
	</div><!-- #primary -->


<?php get_footer(); ?>
