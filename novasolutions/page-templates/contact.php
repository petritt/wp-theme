<?php
/**
 * Template Name: Contact
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div class="container-fluid">

			<div class="row contact-page">
				<div class="col-4 px-0">
					<img class="image_slider" src="<?php the_field('contact_image'); ?>">
				</div>
				<div class="col-md-8">
					<h1 class="text-orange pt-5"><?php the_field('contact_title'); ?></h1>
					<h2  class="text-orange mb-5"><?php the_field('contact_txt'); ?></h2>
					<div class="cf7 py-3">
						<?php echo do_shortcode('[contact-form-7 id="262" title="Contact form 1"]');?>
					</div>
                    <div class="row contact-address">
                        <div class="col-12 col-lg-2 text-left"><h5><b><?php the_field('info_title'); ?></b></h5></div>
                        <div class="col-12 col-lg-4 text-left"><h6><?php the_field('contact_addresses'); ?></h6></div>
                        <div class="col-12 col-lg-4 text-left"><h6><?php the_field('email_and_phone'); ?></h6></div>
                    </div>
				</div>
			</div>
		
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>