<?php
/**
 * Template Name: About us
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<!-- <div id="content" role="main"> -->
			<div class="">
				<div class="row no-gutters aboutUs-banner-wrapper">	
					<div class="col-12 col-md-4 col-lg-4">
						<div class="image-wrap text-left">
							<img class="aboutUs-imgBanner" src="<?= get_field('banner_aboutus');?>" style="margin-top: 40%">
						</div>
					</div>
					<div class="col-12 col-md-7 col-lg-7 offset-md-1">
						<div class="row row-smaller h-100">
							<div class="col-md-12 col-lg-11 col-smaller align-self-center">
								<h1 class="mb-3 mb-lg-4 text-orange paragraph_aboutus"><?= get_field('heading_aboutus'); ?></h1>
								<h2 class="text-orange text-right"><?= get_field('paragraph_aboutus'); ?></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			<section>
				<div class="row no-gutters">
                    <a href="/team" class="col-12 col-md-3 col-lg-3 d-flex justify-content-end align-items-end meetTeam-aboutUs"">
                        <div class="team_wrap">
                            <h4 class="text-white font-weight-bold d-inline-block position_class pt-4 mr-lg-5">
                                <?= get_field('label_meet_the_team');?>
                            </h4>
                        </div>
                    </a>
					<div class="col-12 col-md-9 col-lg-9">
					    <div>
							<img src="<?= get_field('banner_team_aboutus');?>">
						</div>
					</div>
				</div>
			</section>
			<section class="timeline timeline_image_bg" >
				<div class="" style="background-image: url('<?php the_field('timeline_image_bg'); ?>');" >
				<div class="timeline-overlay">
				
					<h2 class="paddingbottom timeline_title pl-5"><?php the_field('timeline_title'); ?></h2>
					<div class="timeline-inside ">
						
							<div class="centerourwork">
								<?php
									if( have_rows('timeline_slider') ):
									$i =1;
										while ( have_rows('timeline_slider') ) : the_row();?>
										<div class="timeline-wrapper">
											<div class="timeline-content">
												<div class="timeline-img">
													<img src="<?php the_sub_field('timeline_slider_image') ?>">
												</div>

                                                <div class="row py-2 timeline-txt">
                                                    <div class="col-12 ">
                                                        <h4 class=""><?php the_sub_field('timeline_slider_content') ?></h4>
                                                    </div>
                                                    <div class="col-12">
                                                        <p class="year"><?php the_sub_field('timeline_slider_year') ?></p>
                                                    </div>

                                                </div>
											</div>
											
										</div>	
									
										<?php $i++; endwhile;
									else :
									// no rows found
									endif;
								?>
								
							</div>
							
					</div>
					<hr />

                    <div id="time_line">
                        <div class="swiper-container">
                            <p class="swiper-control">
                                <button type="button" class="btn btn-default btn-sm prev-slide">Prev</button>
                                <button type="button" class="btn btn-default btn-sm next-slide">Next</button>
                            </p>
                            <div class="swiper-wrapper timeline">
                                <div class="swiper-slide" v-for="item in steps">
                                    <div class="timestamp">
                                        <span class="date">{{item.dateLabel}}<span>
                                    </div>
                                    <div class="status">
                                        <span>{{item.title}}</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>


				</div>
				</div>	
			</section>
			<section class="activities">
				<h2 class="paddingbottom text-grey activities-title pt-5 pl-5"><?php the_field('activities_title'); ?></h2>
				<div class="activities-inside">

                <?php
                $lastposts = get_posts( array(
                    'posts_per_page' => 5
                ) );
                ?>

                    <div class="centerourwork">
                        <?php
                            if ( $lastposts ) {
                                $i = 1;
                                foreach ( $lastposts as $post ) :
                                    setup_postdata( $post ); ?>
                                    <div class="">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="image_wrap">
                                                <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                            </div>
                                            <div class="row py-5 activities-txt">
                                                <div class="col-12 ">
                                                    <p class="text-orange"><?php echo get_the_excerpt(); ?></p>
                                                </div>
                                                <div class="col-12">
                                                    <p class="text-orange pt-5"><b>
                                                            <?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ?>
                                                            / <?php echo str_pad(count($lastposts), 2, '0', STR_PAD_LEFT);  ?>
                                                        </b>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                $i++;
                                endforeach;
                                wp_reset_postdata();
                            }
                        ?>

                    </div>
				
				</div>
                <div class="paginator-center text-left">
                    <i class="prev fas fa-chevron-left"></i>
                    <i class="next fas fa-chevron-right"></i>
                </div>
			</section>
	

		<!-- </div> -->
		<!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
