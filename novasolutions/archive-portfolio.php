<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Novasolutions
 */

get_header();
?>

	<div id="primary" class="container-fluid site-content">
	
		<div id="divWpVue">

            <i v-if="loading" class="fas fa-spinner fa-spin"></i>


            <div class="row py-4">
				<div class="col-lg-3 portfolio_filter">

                    <div id="portfolio_sidebar" class="position-md-fixed">

                        <div class="form-check">
                            <input class="form-check-input option-input checkbox" type="radio" name="exampleRadios" value="All" v-model="category">
                            <label class="form-check-label" for="exampleRadios1">
                                All Projects
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input option-input checkbox" type="radio" name="exampleRadios" value="Social Media" v-model="category">
                            <label class="form-check-label" for="exampleRadios2">
                                Social Media
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input option-input checkbox" type="radio" name="exampleRadios" value="Packaging" v-model="category">
                            <label class="form-check-label" for="exampleRadios2">
                                Packaging
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input option-input checkbox" type="radio" name="exampleRadios" value="Design" v-model="category">
                            <label class="form-check-label" for="exampleRadios2">
                                Design
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input option-input checkbox" type="radio" name="exampleRadios" value="Video Comercial" v-model="category">
                            <label class="form-check-label" for="exampleRadios2">
                                Video Comercial
                            </label>
                        </div>

                    </div>

				</div>
				
				<div class="col-md-9">
				
					<div class="row"> 
					
					<div v-for="(item, index) in filteredServices" :key="index" class="col-md-12 col-lg-6 py-5 portfolio_wrap">

                        <transition name="bounce">

                        <a :href="item.url">
						
							<div class="portfolio">
								<img :src="getImgUrl(item.image)">
							</div>
							
							<h4 class="text-orange font-weight-bold d-inline-block position_class pt-4">{{ item.name }}</h4>
							<p class="card-text text-orange pt-5"><b>{{ ('0' + (parseInt(index)+1)).slice(-2) }} / {{ '0' + (parseInt(info.length)) }}</b></p>

						</a>

                        </transition>

					</div>
					
					</div>
				
				</div>
			</div>
			
		</div>
		
	</div><!-- #primary -->

<?php
get_footer();
