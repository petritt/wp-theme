<?php
/**
 * Novasolutions functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Novasolutions
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'novasolutions_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function novasolutions_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Novasolutions, use a find and replace
		 * to change 'novasolutions' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'novasolutions', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'novasolutions' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'novasolutions_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'novasolutions_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function novasolutions_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'novasolutions_content_width', 640 );
}
add_action( 'after_setup_theme', 'novasolutions_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function novasolutions_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'novasolutions' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'novasolutions' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'novasolutions_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function novasolutions_scripts() {
	wp_enqueue_style( 'novasolutions-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'novasolutions-style', 'rtl', 'replace' );

	wp_enqueue_script( 'novasolutions-main', get_template_directory_uri() . '/js/main.js', array(), _S_VERSION, true );
	
	wp_enqueue_script( 'novasolutions_vuejs', 'https://unpkg.com/vue@2.6.12/dist/vue.min.js', array());


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'novasolutions_scripts' );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * Register Custom Post Team Type 
 */
function add_team_init() {
    $labels = array(
        'name'                  => _x( 'Team', 'Post type general name', 'uklo' ),
        'singular_name'         => _x( 'Team', 'Post type singular name', 'uklo' ),
        'menu_name'             => _x( 'Team', 'Admin Menu text', 'uklo' ),
        'name_admin_bar'        => _x( 'Team', 'Add New on Toolbar', 'uklo' ),
        'add_new'               => __( 'Add New', 'uklo' ),
        'add_new_item'          => __( 'Add New Team member', 'uklo' ),
        'new_item'              => __( 'New Team', 'uklo' ),
        'edit_item'             => __( 'Edit Team', 'uklo' ),
        'view_item'             => __( 'View Team', 'uklo' ),
        'all_items'             => __( 'All Team members', 'uklo' ),
        'search_items'          => __( 'Search Team members', 'uklo' ),
        'parent_item_colon'     => __( 'Parent Team members:', 'uklo' ),
        'not_found'             => __( 'No Team members found.', 'uklo' ),
        'not_found_in_trash'    => __( 'No Team members found in Trash.', 'uklo' ),
        'featured_image'        => _x( 'Professor Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'uklo' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'archives'              => _x( 'Team archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'uklo' ),
        'insert_into_item'      => _x( 'Insert into Team', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'uklo' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Team', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'uklo' ),
        'filter_items_list'     => _x( 'Filter Team list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'uklo' ),
        'items_list_navigation' => _x( 'Team list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'uklo' ),
        'items_list'            => _x( 'Team list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'uklo' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
		'show_in_rest'       => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'team' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail'),
		'taxonomies'         => array( 'team_category' ),
    );
 
    register_post_type( 'team', $args );
}
 
add_action( 'init', 'add_team_init' );

/**
 * Register Custom Post Portfolio Type 
 */
function add_portfolio_init() {
    $labels = array(
        'name'                  => _x( 'Portfolio', 'Post type general name', 'uklo' ),
        'singular_name'         => _x( 'Portfolio', 'Post type singular name', 'uklo' ),
        'menu_name'             => _x( 'Portfolio', 'Admin Menu text', 'uklo' ),
        'name_admin_bar'        => _x( 'Portfolio', 'Add New on Toolbar', 'uklo' ),
        'add_new'               => __( 'Add New', 'uklo' ),
        'add_new_item'          => __( 'Add New Portfolio item', 'uklo' ),
        'new_item'              => __( 'New Portfolio', 'uklo' ),
        'edit_item'             => __( 'Edit Portfolio', 'uklo' ),
        'view_item'             => __( 'View Portfolio', 'uklo' ),
        'all_items'             => __( 'All Portfolio items', 'uklo' ),
        'search_items'          => __( 'Search Portfolio items', 'uklo' ),
        'parent_item_colon'     => __( 'Parent Portfolio items:', 'uklo' ),
        'not_found'             => __( 'No Portfolio items found.', 'uklo' ),
        'not_found_in_trash'    => __( 'No Portfolio items found in Trash.', 'uklo' ),
        'featured_image'        => _x( 'Portfolio Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'uklo' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'uklo' ),
        'archives'              => _x( 'Portfolio archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'uklo' ),
        'insert_into_item'      => _x( 'Insert into Portfolio', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'uklo' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Portfolio', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'uklo' ),
        'filter_items_list'     => _x( 'Filter Portfolio list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'uklo' ),
        'items_list_navigation' => _x( 'Portfolio list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'uklo' ),
        'items_list'            => _x( 'Portfolio list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'uklo' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
		'show_in_rest'       => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail'),
		'taxonomies'          => array( 'category' ),
    );
 
    register_post_type( 'portfolio', $args );
}
 
add_action( 'init', 'add_portfolio_init' );


/**
 * Custom walker class.
 */
class WPDocs_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
        $indent = str_repeat( "\t", $depth );

        // Select a CSS class for this `<ul>` based on $depth
        switch( $depth ) {
            case 0:
                // Top-level submenus get the 'nav-main-sub-list' class
                $class = 'nav-main-sub-list';
                break;
            case 1:
            case 2:
            case 3:
                // Submenus nested 1-3 levels deep get the 'nav-other-sub-list' class
                $class = 'sub-list';
                break;
            default:
                // All other submenu `<ul>`s receive no class
                break;
        }

        // Only print out the 'class' attribute if a class has been assigned
        if( isset( $class ) )
            $output .= "\n$indent<ul class=\"$class\">\n";
        else
            $output .= "\n$indent<ul>\n";
    }

    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $indent = str_repeat("\t", $depth);
        $attributes  = '';

        ! empty ( $item->attr_title )
        // Avoid redundant titles
        and $item->attr_title !== $item->title
        and $attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';

        ! empty ( $item->url )
        and $attributes .= ' href="' . esc_attr( $item->url ) .'"';

        $attributes  = trim( $attributes );
        $title       = apply_filters( 'the_title', $item->title, $item->ID );
        $item_output = "$args->before<a $attributes>$args->link_before$title</a>"
            . "$args->link_after$args->after";

        $optionalClass = '';
        if($item->classes[0] !== '' &&  $item->classes[0] !== 'Menu')
            $optionalClass = $item->classes[0];

        // Select a CSS class for this `<li>` based on $depth
        switch( $depth ) {
            case 0:
                // Top-level `<li>`s get the 'nav-main-item' class
                $class = 'main-li';
                break;
            case 1:
                $class = 'sub-li';
                break;
            default:
                // All other `<li>`s receive no class
                break;
        }

        // Only print out the 'class' attribute if a class has been assigned
        if( isset( $class ) )
            $output .= $indent . '<li class="'. $class . ' ' . $optionalClass . '">';
        else

            $output .= $indent . '<li>';

        $output .= apply_filters(
            'walker_nav_menu_start_el',
            $item_output,
            $item,
            $depth,
            $args
        );
    }
}


/**
 * Defer scripts
 *
 * With this function we defer the scripts so the website loads first
 * and dont wait for the scripts to load
 *
 */
add_filter( 'script_loader_tag', function ( $tag, $handle ) {

    switch ($handle) {
        case "novasolutions-main":
            return str_replace( 'src', 'defer="defer" src', $tag );

        case "contact-form-7":
            return str_replace( 'src', 'defer="defer" src', $tag );

        case "wp-embed":
            return str_replace( 'src', 'defer="defer" src', $tag );

        case "novasolutions_vuejs":
            return str_replace( 'src', 'defer="defer" src', $tag );

        case "hoverintent-js":
            return str_replace( 'src', 'defer="defer" src', $tag );

        default: return $tag;
    }

}, 10, 2 );

function add_font_awesome_5_cdn_attributes( $html, $handle ) {

    if ( 'contact-form-7' === $handle ) {
        return str_replace( "rel='stylesheet'", "rel='preload' as='style'", $html );
    }

    if ( 'wp-block-library' === $handle ) {
        return str_replace( "rel='stylesheet'", "rel='preload' as='style'", $html );
    }
    return $html;
}
add_filter( 'style_loader_tag', 'add_font_awesome_5_cdn_attributes', 10, 2 );

/**
 * Filter the excerpt length to 30 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wp_example_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wp_example_excerpt_length');

function wporg_fcs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wporg_fcs_excerpt_more' );