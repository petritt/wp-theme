<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Novasolutions
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="shortcut icon" type="image/ico" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site"> 

	<header id="masthead" class="site-header">

        <div class="bg-white fixed-top">
            <div class="container">
                <div class="d-flex h-100 justify-content-between align-items-sm-center flex-column flex-sm-row">

                    <div class="logo pt-3 pb-2">
                        <?php if ( the_custom_logo() ) : ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_custom_logo(); ?>"
                                class="image-auto" width="<?php echo esc_attr( get_custom_header()->width ); ?>"
                                height="<?php echo esc_attr( get_custom_header()->height ); ?>"
                                alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
                            </a>
                        <?php endif; ?>
                    </div>


                    <span class="navbar-nav ml-auto align-items-center mb-0" onclick="openNav()">
                        <span class="hamburger-menu">
                        </span>
                    </span>

                    <div id="myNav" class="overlay">

                        <a href="javascript:void(0)" class="closebtn_text d-inline-block" onclick="closeNav()">Close</a>
                        <a href="javascript:void(0)" class="closebtn d-inline-block" onclick="closeNav()">&times;</a>

                        <?php

                            wp_nav_menu( array( 'theme_location' => 'menu-1',
                                'menu'                 => '',
                                'container'            => 'nav',
                                'container_class'      => 'navbar-nav ml-auto align-items-center mb-0',
                                'container_id'         => '',
                                'container_aria_label' => '',
                                'menu_class'           => 'overlay-content',
                                'menu_id'              => '',
                                'echo'                 => true,
                                'fallback_cb'          => 'wp_page_menu',
                                'before'               => '',
                                'after'                => '',
                                'link_before'          => '',
                                'link_after'           => '',
                                   'items_wrap' => '<ul class="%2$s">%3$s</ul>',
                                'item_spacing'         => 'preserve',
                                'depth'                => 0,
                                'walker' => new WPDocs_Walker_Nav_Menu()
                            ) );
                        ?>

                        <div class="menu-bottom pl-5">
                            <div class="border-menu"></div>
                            <div class="social pt-4 w-100">
                                <ul class="list-inine social_header pl-0 mb-0">
                                    <li class="list-inline-item py-1">
                                        <a href="https://www.facebook.com/nova.solutions" target="_blank">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item py-1">
                                        <a href="https://www.instagram.com/nova_solutions/" target="_blank">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item py-1">
                                        <a href="https://www.linkedin.com/company/nova-solutions/" target="_blank">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="privacy pt-4 pb-5 w-100">
                                <ul class="list-inine pl-0 mb-0">
                                    <li class="list-inline-item py-1"><a href="#">Privacy Policy</a></li>
                                    <li class="list-inline-item py-1"><a href="#">Tearms & Conditions</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>


                </div>

            </div>

        </div>


	</header><!-- #masthead -->
