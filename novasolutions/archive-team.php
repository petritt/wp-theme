<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Novasolutions
 */

get_header();
?>

	<div id="primary" class="site-content">
		<div id="content" class="container-fluid team" role="main">
			<div class="team-bannerImg row">
<!--				<div class="">-->
					<img src="/wp-content/uploads/2020/10/TheTeam_Picture.png" alt="The Team">
<!--				</div>-->
			</div>

            <div class="container">
                <div class="row ">
                    <div class="col-12 ">
                        <h1 class="text-orange text-right team-header-text mb-0">Meet our executive team</h1>
                    </div>
                    <div class="col-12">
                        <h2 class="text-orange text-right team-header-subtext">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse.
                        </h2>
                    </div>
                </div>
            </div>

		</div>

		<?php 

			$args = array(
				'numberposts'	=> -1,
				'post_type'		=> 'team',
				'order' 		=> 'ASC',
			);

			$the_query = new WP_Query( $args );

		?>

			
		<section class="container team-container">

			<div class="row team-wrapper justify-content-center">
			
			<?php if( $the_query->have_posts() ): ?>
			
				<?php $i = 0; ?>
				<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
					<div class="card team-card team_wrap">
						<img class="card-img-top employee-image" src="<?= the_field('employee_image'); ?>">
						<img class="card-img-top employee-image-hover" src="<?= the_field('employee_image_hover'); ?>">
						<div class="card-body team-card-body">
							<h5 class="card-title text-orange pt-5"><?php the_title(); ?></h5>
							<small class="card-text text-orange"><?= the_field('employee_position'); ?></small>
							<hr class="mt-4" />
							<p class="card-text text-orange pt-5"><b><?php echo str_pad($i+1, 2, '0', STR_PAD_LEFT); ?> / <?php echo str_pad($the_query->post_count, 2, '0', STR_PAD_LEFT); ?></b></p>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</section>
		
	</div><!-- #primary -->

<?php
get_footer();
