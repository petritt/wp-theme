<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Novasolutions
 */

?>

<article id="post-<?php the_ID(); ?>" class="container-fluid portfolio_class">

	<div class="row">

		<div class="col-3">

		</div>
		
		<div class="col-md-9">


			<div class="row">
			
				<div class="col-12 py-4">
					<div class="feature_portfolio">
						<img src="<?php echo  get_the_post_thumbnail_url(get_the_ID(),'full');   ?>">
					</div>
				</div>

                <div class="col-12">
                    <?php the_title( '<h1 class="text-orange">', '</h1>' ); ?>
                </div>

				<div class="col-12 py-4">
					<?php the_content(); ?>
				</div>

				
			</div>
			
			<h5 class="text-orange text-right team-header-text">Relevant Projects</h5>
			
			 <?php

			$relatedPortfolio = get_posts( 
				array( 'category__in' => wp_get_post_categories(get_the_ID()), 
				'numberposts' => 2, 
				'post_type' => 'portfolio', 
				'post__not_in' => array(get_the_ID()) 
			));
			
			if( $relatedPortfolio ) ?>
			
			<div class="row">	
			<?php 
			$i = 0;
			foreach( $relatedPortfolio as $post ) { ?>
			
				<?php 
				setup_postdata($post); ?>
						<div class="col-5 portfolio_wrap <?php echo $i === intval(0) ? 'ml-auto' : '' ?>">
							<a href="<?php the_permalink() ?>">
								<div class="portfolio">
									<?php  the_post_thumbnail(); ?>
								</div>

								<h4 class="text-orange font-weight-bold d-inline-block position_class pt-4"><?php the_title(); ?></h4>
							</a>
						</div>
					
				<?php $i++; }
			
			wp_reset_postdata(); ?>
			
			</div>
		
		
		</div>

	
	</div>


</article><!-- #post-<?php the_ID(); ?> -->
