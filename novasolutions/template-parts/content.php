<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Novasolutions
 */

?>

<article id="post-<?php the_ID(); ?>"  class="container-fluid">



    <div class="row">
        <div class="col-12">
            <div class="feature_portfolio">
                <img src="<?php echo  get_the_post_thumbnail_url(get_the_ID(),'full');   ?>">
            </div>

            <div class="team-wrapper py-5">
                <?php the_title( '<h1 class="text-orange">', '</h1>' ); ?>

                <div class="py-4">
                    <?php the_content(); ?>
                </div>
            </div>


        </div>
    </div>


</article><!-- #post-<?php the_ID(); ?> -->
